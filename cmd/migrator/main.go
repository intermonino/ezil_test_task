package main

import (
	"log"
	"os"
	"strings"

	"ezil/internal/config"
	"ezil/internal/infra/psql"
)

const (
	migrationCmdUp   = "up"
	migrationCmdDown = "down"
)

func main() {
	if len(os.Args) != 2 {
		log.Fatalf("Invalid args count")
	}

	conf, err := config.InitFromEnv()
	if err != nil {
		log.Fatalf("Config init error: %v", err)
	}

	cmd := strings.ToLower(os.Args[1])
	switch cmd {
	case migrationCmdUp:
		err = psql.MigrateUp(conf.Postgres.MasterDSN)
		if err != nil {
			log.Fatalf("Migrate up error: %v", err)
		}
	case migrationCmdDown:
		err = psql.MigrateDown(conf.Postgres.MasterDSN)
		if err != nil {
			log.Fatalf("Migrate up error: %v", err)
		}
	}
	log.Println("Migrated successfully")
}
