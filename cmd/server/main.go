package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/rs/zerolog"

	"ezil/internal/config"
	"ezil/internal/infra/metrics"
	"ezil/internal/server"
	"ezil/pkg/utils"
)

func main() {
	conf, err := config.InitFromEnv()
	if err != nil {
		log.Fatalf("Config init error: %v", err)
	}

	l := zerolog.New(os.Stdout).Level(zerolog.InfoLevel)
	if conf.Debug {
		l = l.Level(zerolog.DebugLevel)
	}
	l.Info().Interface("config", conf).Msg("Start init server")
	ctx, cancel := context.WithCancel(context.Background())
	go utils.InitGracefulShutdown(cancel)

	srv := server.New(fmt.Sprintf(":%s", conf.ServerPort),
		l.With().Str("module", "http-server").Logger())

	mserv := metrics.NewMetricServer(conf.MetricsPort, conf.EnableProfiling)
	go mserv.Start(ctx)

	srv.Run(ctx)
}
