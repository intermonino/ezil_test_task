package main

import (
	"context"
	"log"
	"os"

	"github.com/rs/zerolog"

	"ezil/internal/config"
	"ezil/internal/infra/metrics"
	"ezil/internal/infra/psql"
	man_repo "ezil/internal/repository/manufacturer/psql"
	"ezil/internal/usecase/manufacturer"
	"ezil/internal/worker"
	"ezil/pkg/utils"
)

func main() {
	conf, err := config.InitFromEnv()
	if err != nil {
		log.Fatalf("Config init error: %v", err)
	}

	l := zerolog.New(os.Stdout).Level(zerolog.InfoLevel)
	if conf.Debug {
		l = l.Level(zerolog.DebugLevel)
	}
	l.Info().Interface("config", conf).Msg("Start init worker")
	ctx, cancel := context.WithCancel(context.Background())
	go utils.InitGracefulShutdown(cancel)

	l.Info().Msg("Try to connect to psql")
	master, err := psql.Init(ctx, conf.Postgres.MasterDSN)
	if err != nil {
		l.Fatal().Err(err).Msg("psql connection init error")
	}

	l.Info().Msg("Try to init repository")
	manRepo := man_repo.New(master, l.With().Str("module", "repository").Logger())

	l.Info().Msg("Try to init usecase")
	uc := manufacturer.New(manRepo, l.With().Str("module", "usecase").Logger())

	l.Info().Msg("Try to init worker")
	w := worker.New(uc, conf, l)

	mserv := metrics.NewMetricServer(conf.MetricsPort, conf.EnableProfiling)
	go mserv.Start(ctx)
	w.Run(ctx)
}
