package server

import (
	"context"
	"math/rand"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"

	"ezil/internal/models"
)

type Server struct {
	httpServer *http.Server
	logger     *zerolog.Logger
}

//easyjson:json
type UpdateRequest struct {
	Ms []*models.Manufacturer `json:"manufacturers"`
}

func New(addr string, l zerolog.Logger) *Server {
	r := gin.New()
	r.Use(gin.Recovery())
	r.Use(LoggerAndMetrics(l.With().Str("module", "http-middleware").Logger()))

	r.POST("/update", func(c *gin.Context) {
		r := rand.Intn(10)
		time.Sleep(time.Duration(r) * time.Millisecond)

		req := &UpdateRequest{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		for _, m := range req.Ms {
			m.Details[models.IsNeedUpdateField] = false
		}
		c.JSON(http.StatusOK, req)
	})

	return &Server{
		httpServer: &http.Server{Addr: addr, Handler: r},
		logger:     &l,
	}
}

func (s *Server) Run(ctx context.Context) {
	go func() {
		<-ctx.Done()
		if err := s.httpServer.Shutdown(ctx); err != nil {
			s.logger.Fatal().Err(err).Msg("Server forced to shutdown")
		}
		s.logger.Info().Msg("Server gracefully shutdown")
	}()

	if err := s.httpServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		s.logger.Fatal().Err(err).Msg("ListenAndServe error")
	}
}
