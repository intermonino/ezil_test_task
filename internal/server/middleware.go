package server

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
)

func LoggerAndMetrics(l zerolog.Logger) func(c *gin.Context) {
	return func(c *gin.Context) {
		t := time.Now()

		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery

		c.Next()

		latency := time.Since(t)
		clientIP := c.ClientIP()
		method := c.Request.Method
		statusCode := c.Writer.Status()
		if raw != "" {
			path = path + "?" + raw
		}
		msg := c.Errors.String()
		if msg == "" {
			msg = "Request"
		}

		switch {
		case statusCode >= http.StatusBadRequest && statusCode < http.StatusInternalServerError:
			l.Warn().Str("method", method).Str("path", path).Dur("resp_time", latency).Int("status", statusCode).Str("client_ip", clientIP).Msg(msg)
		case statusCode >= http.StatusInternalServerError:
			l.Error().Str("method", method).Str("path", path).Dur("resp_time", latency).Int("status", statusCode).Str("client_ip", clientIP).Msg(msg)
		default:
			l.Info().Str("method", method).Str("path", path).Dur("resp_time", latency).Int("status", statusCode).Str("client_ip", clientIP).Msg(msg)
		}
	}
}
