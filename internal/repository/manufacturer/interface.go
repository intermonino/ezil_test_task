package manufacturer

import (
	"context"

	"ezil/internal/models"
)

type Repository interface {
	CreateManufacturers(ctx context.Context, ms []*models.Manufacturer) error
	GetManufacturersByNeedUpdate(ctx context.Context, isNeedUpdate bool, limit uint64) ([]*models.Manufacturer, error)
	UpdateManufacturers(ctx context.Context, ms []*models.Manufacturer) error
}
