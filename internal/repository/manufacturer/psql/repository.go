package psql

import (
	"context"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/zerolog"

	"ezil/internal/infra/metrics"
	"ezil/internal/models"
)

const (
	manufacturerTableName = "manufacturer"
)

type Repository struct {
	master *pgxpool.Pool
	// slave  *pgxpool.Pool
	logger *zerolog.Logger
}

func New(master *pgxpool.Pool, logger zerolog.Logger) *Repository {
	return &Repository{
		master: master,
		logger: &logger,
	}
}

func (r *Repository) CreateManufacturers(ctx context.Context, ms []*models.Manufacturer) error {
	l := r.logger.With().Str("method", "CreateManufacturers").Logger()
	baseQuery := sq.Insert(manufacturerTableName).Columns("details")
	query := baseQuery.PlaceholderFormat(sq.Dollar)
	for _, m := range ms {
		query = query.Values(m.Details)
	}
	sql, args, err := query.ToSql()
	if err != nil {
		l.Error().Err(err).Msg("sql building error")
		return err
	}
	start := time.Now()
	defer func() {
		metrics.WorkerMetric.SetPostgresQueryLatency(start, baseQuery.Values(), err)
	}()
	_, err = r.master.Exec(ctx, sql, args...)
	if err != nil {
		r.logger.Error().Err(err).Msg("sql execution error")
		return err
	}
	return nil
}

func (r *Repository) UpdateManufacturers(ctx context.Context, ms []*models.Manufacturer) (err error) {
	l := r.logger.With().Str("method", "UpdateManufacturers").Logger()

	tx, err := r.master.Begin(ctx)
	if err != nil {
		l.Error().Err(err).Msg("tx creating error")
		return err
	}

	defer func() {
		if err != nil {
			// nolint:errcheck // reason
			tx.Rollback(ctx)
			return
		}
		err = tx.Commit(ctx)
	}()

	baseQuery := sq.Update(manufacturerTableName).PlaceholderFormat(sq.Dollar)
	start := time.Now()
	defer func() {
		metrics.WorkerMetric.SetPostgresQueryLatency(start, baseQuery.Set("details", ""), err)
	}()

	for _, m := range ms {
		q := baseQuery.Set("details", m.Details)
		sql, args, err := q.ToSql()
		if err != nil {
			l.Err(err).Msg("sql building error")
			return err
		}
		_, err = tx.Exec(ctx, sql, args...)
		if err != nil {
			l.Error().Err(err).Msg("sql execution error")
			return err
		}
	}

	return nil
}

func (r *Repository) GetManufacturersByNeedUpdate(ctx context.Context, isNeedUpdate bool, limit uint64) ([]*models.Manufacturer, error) {
	l := r.logger.With().Str("method", "GetManufacturersByNeedUpdate").Logger()
	query := sq.Select("uuid, details").From(manufacturerTableName).
		Where(sq.Eq{"details -> 'isNeedUpdate'": isNeedUpdate}).Where("deleted_at is NULL").PlaceholderFormat(sq.Dollar)

	if limit > 0 {
		query = query.Limit(limit)
	}

	sql, args, err := query.ToSql()
	if err != nil {
		l.Error().Err(err).Msg("sql building error")
		return nil, err
	}

	start := time.Now()
	defer func() {
		metrics.WorkerMetric.SetPostgresQueryLatency(start, query, err)
	}()

	l.Debug().Str("sql", sql).Msg("query")
	rows, err := r.master.Query(ctx, sql, args...)
	if err != nil {
		l.Error().Err(err).Msg("sql execution error")
		return nil, err
	}

	defer rows.Close()
	res := make([]*models.Manufacturer, 0)
	for rows.Next() {
		m := &models.Manufacturer{}
		err = rows.Scan(&m.UUID, &m.Details)
		if err != nil {
			l.Error().Err(err).Msg("scan error")
			return nil, err
		}
		res = append(res, m)
	}
	if err = rows.Err(); err != nil {
		l.Error().Err(err).Msg("row error")
		return nil, err
	}
	if len(res) == 0 {
		l.Error().Err(pgx.ErrNoRows).Msg("empty result error")
		return nil, pgx.ErrNoRows
	}
	return res, nil
}
