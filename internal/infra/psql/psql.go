package psql

import (
	"context"
	"fmt"
	"os"
	"path"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jackc/pgx/v4/pgxpool"
)

func Init(ctx context.Context, dsn string) (*pgxpool.Pool, error) {
	pool, err := pgxpool.Connect(ctx, dsn)
	if err != nil {
		return nil, err
	}
	return pool, nil
}

func MigrateUp(repoDSN string) error {
	rootDir, _ := os.Getwd()

	migrationsPath := "file://" + path.Join(rootDir, "deploy/migrations/psql")

	m, err := migrate.New(migrationsPath, repoDSN)
	if err != nil {
		return fmt.Errorf("unable to prepare migration: %w", err)
	}

	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		return fmt.Errorf("unable to perform migration: %w", err)
	}

	return nil
}

func MigrateDown(repoDSN string) error {
	rootDir, _ := os.Getwd()

	migrationsPath := "file://" + path.Join(rootDir, "migrations/psql")

	m, err := migrate.New(migrationsPath, repoDSN)
	if err != nil {
		return fmt.Errorf("unable to prepare migration: %w", err)
	}

	if err := m.Down(); err != nil && err != migrate.ErrNoChange {
		return fmt.Errorf("unable to perform migration: %w", err)
	}

	return nil
}
