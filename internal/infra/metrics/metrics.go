package metrics

import (
	"time"

	"github.com/Masterminds/squirrel"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var WorkerMetric *WorkerMetrics

func init() {
	WorkerMetric = NewWorkerMetrics()
}

type WorkerMetrics struct {
	CountOfGeneratedManufacturers prometheus.Counter
	CountOfUpdatedManufacturers   prometheus.Counter
	UpdateRequestLatency          prometheus.Histogram
	PostgresQueryLatency          prometheus.HistogramVec
}

type ServerMetrics struct {
	CountOfRequests prometheus.CounterVec
	RequestLatency  prometheus.HistogramVec
}

func NewWorkerMetrics() *WorkerMetrics {
	return &WorkerMetrics{
		CountOfGeneratedManufacturers: promauto.NewCounter(prometheus.CounterOpts{
			Name: "count_of_generated_manufactures",
			Help: "Count of generated manufactures",
		}),
		CountOfUpdatedManufacturers: promauto.NewCounter(prometheus.CounterOpts{
			Name: "count_of_updated_manufactures",
			Help: "Count of updated manufactures",
		}),

		UpdateRequestLatency: promauto.NewHistogram(prometheus.HistogramOpts{
			Name: "update_request_latency",
			Help: "Time latency of update request",
		}),

		PostgresQueryLatency: *promauto.NewHistogramVec(prometheus.HistogramOpts{
			Name:    "postgres_query_latency",
			Help:    "Time latency of psql queries",
			Buckets: prometheus.LinearBuckets(100000, 100000, 100),
		}, []string{"sql", "error"}),
	}

}

func (m *WorkerMetrics) RegisterMetrics(reg prometheus.Registerer) {
	reg.MustRegister(m.CountOfGeneratedManufacturers)
	reg.MustRegister(m.CountOfUpdatedManufacturers)
	reg.MustRegister(m.UpdateRequestLatency)
	reg.MustRegister(m.PostgresQueryLatency)
}

func (m *WorkerMetrics) Close() {
	prometheus.Unregister(m.CountOfGeneratedManufacturers)
	prometheus.Unregister(m.CountOfUpdatedManufacturers)
	prometheus.Unregister(m.UpdateRequestLatency)
	prometheus.Unregister(m.PostgresQueryLatency)
}

func (m *WorkerMetrics) SetUpdateRequestLatency(startAt time.Time) {
	dur := time.Since(startAt)
	m.UpdateRequestLatency.Observe(float64(dur.Milliseconds()))
}

func (m *WorkerMetrics) SetPostgresQueryLatency(startAt time.Time, q squirrel.Sqlizer, err error) {
	sql, _, _ := q.ToSql()
	dur := time.Since(startAt)
	labels := prometheus.Labels{"sql": sql, "error": "none"}
	if err != nil {
		labels["error"] = err.Error()
	}
	m.PostgresQueryLatency.With(labels).Observe(float64(dur.Nanoseconds()))
}
