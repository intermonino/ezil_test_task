package metrics

import (
	"context"
	"errors"
	"expvar"
	"net/http"
	"net/http/pprof"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const (
	defaultHTTPReadTimeout  = 15 * time.Second
	defaultHTTPWriteTimeout = 15 * time.Second
)

type Service struct {
	srv *http.Server
}

func NewMetricServer(port string, enableProfiling bool) *Service {
	router := http.NewServeMux()
	router.Handle("/metrics", promhttp.Handler())

	if enableProfiling {
		registerPprof(router)
	}

	return &Service{
		srv: &http.Server{
			Addr:         ":" + port,
			Handler:      router,
			ReadTimeout:  defaultHTTPReadTimeout,
			WriteTimeout: defaultHTTPWriteTimeout,
		},
	}
}

func (s *Service) Start(ctx context.Context) error {
	defer func() {
		<-ctx.Done()
		s.srv.Shutdown(ctx)
	}()

	if err := s.srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		return err
	}
	return nil
}

func registerPprof(r *http.ServeMux) {
	r.HandleFunc("/debug/pprof/profile", pprof.Profile)
	r.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	r.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	r.HandleFunc("/debug/pprof/trace", pprof.Trace)
	r.HandleFunc("/debug/pprof/", pprof.Index)
	r.Handle("/debug/vars", expvar.Handler())
}
