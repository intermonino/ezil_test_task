package models

import (
	"database/sql/driver"
	"encoding/json"
	"errors"

	"github.com/google/uuid"
)

const IsNeedUpdateField = "isNeedUpdate"

//easyjson:json
type Manufacturer struct {
	UUID    uuid.UUID `json:"uuid"`
	Details JSON      `json:"details"`
}

type JSON map[string]interface{}

func (a JSON) Value() (driver.Value, error) {
	return json.Marshal(a)
}

func (a *JSON) Scan(value interface{}) error {
	b, ok := value.([]byte)
	if !ok {
		return errors.New("type assertion to []byte failed")
	}

	return json.Unmarshal(b, &a)
}
