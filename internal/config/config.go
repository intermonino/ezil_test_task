package config

import (
	"fmt"
	"time"

	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	Postgres struct {
		DBHost string `envconfig:"DB_HOST" default:"0.0.0.0:3457"`
		DBUser string `envconfig:"DB_USER" default:"worker"`
		DBPass string `envconfig:"DB_PASS" default:"qwerty"`
		DBSSL  string `envconfig:"DB_SSL" default:"disable"`
		DBName string `envconfig:"DB_NAME" default:"ezil"`

		MasterDSN string
	}

	Debug      bool   `envconfig:"DEBUG" default:"true"`
	ServerHost string `envconfig:"SERVER_HOST" default:"0.0.0.0"`
	ServerPort string `envconfig:"SERVER_PORT" default:"8090"`

	UpdateDuration   time.Duration `envconfig:"UPDATE_DURATION" default:"30s"`
	GenerateDuration time.Duration `envconfig:"GENERATE_DURATION" default:"15s"`
	BatchSize        uint64        `envconfig:"BATCH_SIZE" default:"50"`

	MetricsPort     string `envconfig:"METRICS_PORT" default:"5050"`
	EnableProfiling bool   `envconfig:"ENABLE_PROFILING" default:"true"`
}

func InitFromEnv() (*Config, error) {
	c := &Config{}
	err := envconfig.Process("", c)
	if err != nil {
		return nil, err
	}

	c.Postgres.MasterDSN = fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=%s",
		c.Postgres.DBUser,
		c.Postgres.DBPass,
		c.Postgres.DBHost,
		c.Postgres.DBName,
		c.Postgres.DBSSL,
	)

	return c, nil
}
