package worker

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"time"

	"github.com/rs/zerolog"

	"ezil/internal/config"
	"ezil/internal/infra/metrics"
	"ezil/internal/server"
	"ezil/internal/usecase/manufacturer"
)

type Worker struct {
	generateDuration time.Duration
	updateDuration   time.Duration

	useCase manufacturer.UseCase

	batchSize uint64
	servAddr  string

	logger *zerolog.Logger
}

func New(useCase manufacturer.UseCase, conf *config.Config, l zerolog.Logger) *Worker {
	return &Worker{
		generateDuration: conf.GenerateDuration,
		updateDuration:   conf.UpdateDuration,
		useCase:          useCase,
		batchSize:        conf.BatchSize,
		servAddr:         fmt.Sprintf("http://%s:%s", conf.ServerHost, conf.ServerPort),
		logger:           &l,
	}
}

func (w *Worker) Run(ctx context.Context) {
	genTick := time.NewTicker(w.generateDuration)
	updTick := time.NewTicker(w.updateDuration)
	for {
		select {
		case <-ctx.Done():
			w.logger.Info().Msg("Worker gracefully stopped")
			return
		case <-updTick.C:
			if err := w.tryToUpdateManufacturers(ctx); err != nil {
				w.logger.Error().Err(err).Msg("tryToUpdateManufacturers error")
			} else {
				w.logger.Info().Msg("Manufacturers updated successfully")
			}
		case <-genTick.C:
			if err := w.tryToGenerateManufacturers(ctx); err != nil {
				w.logger.Error().Err(err).Msg("tryToGenerateManufacturers error")
			} else {
				w.logger.Info().Msg("Manufacturers generated successfully")
			}
		}
	}
}

func (w *Worker) tryToUpdateManufacturers(ctx context.Context) error {
	ms, err := w.useCase.GetManufacturersByNeedUpdate(ctx, true, w.batchSize)
	if err != nil {
		return err
	}
	req := server.UpdateRequest{Ms: ms}
	body, err := req.MarshalJSON()
	if err != nil {
		return err
	}
	start := time.Now()
	resp, err := http.Post(w.servAddr+"/update", "application/json; charset=UTF-8", bytes.NewBuffer(body))
	if err != nil {
		return err
	}
	metrics.WorkerMetric.SetUpdateRequestLatency(start)
	defer resp.Body.Close()
	body, err = io.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	res := &server.UpdateRequest{}
	if err := res.UnmarshalJSON(body); err != nil {
		return err
	}

	metrics.WorkerMetric.CountOfUpdatedManufacturers.Add(float64(len(res.Ms)))
	if err := w.useCase.UpdateManufacturers(ctx, res.Ms); err != nil {
		return err
	}
	return nil
}

func (w *Worker) tryToGenerateManufacturers(ctx context.Context) error {
	rand.Seed(time.Now().UnixNano())
	// nolint:gosec // some reason
	count := rand.Intn(int(w.batchSize)) + 1
	if err := w.useCase.CreateRandomManufacturers(ctx, count); err != nil {
		return err
	}
	metrics.WorkerMetric.CountOfGeneratedManufacturers.Add(float64(count))
	return nil
}
