package manufacturer

import (
	"context"

	"ezil/internal/models"
)

type UseCase interface {
	CreateRandomManufacturers(ctx context.Context, count int) error
	GetManufacturersByNeedUpdate(ctx context.Context, isNeedUpdate bool, limit uint64) ([]*models.Manufacturer, error)
	UpdateManufacturers(ctx context.Context, ms []*models.Manufacturer) error
}
