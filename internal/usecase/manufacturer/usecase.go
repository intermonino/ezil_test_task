package manufacturer

import (
	"context"

	"github.com/rs/zerolog"

	faker "github.com/brianvoe/gofakeit/v6"

	"ezil/internal/models"
	"ezil/internal/repository/manufacturer"
)

type Imp struct {
	manRepo manufacturer.Repository

	logger *zerolog.Logger
}

func New(manRepo manufacturer.Repository, l zerolog.Logger) *Imp {
	return &Imp{
		manRepo: manRepo,
		logger:  &l,
	}
}

func (uc *Imp) CreateRandomManufacturers(ctx context.Context, count int) error {
	l := uc.logger.With().Str("method", "CreateRandomManufacturers").Logger()
	ms := make([]*models.Manufacturer, 0, count)
	for i := 0; i < count; i++ {
		m := &models.Manufacturer{
			Details: faker.Map(),
		}
		m.Details["isNeedUpdate"] = faker.Bool()
		ms = append(ms, m)
	}
	err := uc.manRepo.CreateManufacturers(ctx, ms)
	if err != nil {
		l.Err(err).Msg("CreateManufacturers error")
		return err
	}
	return nil
}

func (uc *Imp) GetManufacturersByNeedUpdate(ctx context.Context, isNeedUpdate bool, limit uint64) ([]*models.Manufacturer, error) {
	l := uc.logger.With().Str("method", "GetManufacturersByNeedUpdate").Logger()

	ms, err := uc.manRepo.GetManufacturersByNeedUpdate(ctx, isNeedUpdate, limit)
	if err != nil {
		l.Err(err).Msg("CreateManufacturers error")
		return nil, err
	}
	return ms, nil
}

func (uc *Imp) UpdateManufacturers(ctx context.Context, ms []*models.Manufacturer) error {
	l := uc.logger.With().Str("method", "UpdateManufacturers").Logger()
	err := uc.manRepo.UpdateManufacturers(ctx, ms)
	if err != nil {
		l.Err(err).Msg("UpdateManufacturers error")
		return nil
	}
	return err
}
