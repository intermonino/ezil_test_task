.PHONY: migrate_up
migrate_up:
	docker run -v ${PWD}/deploy/migrations/psql:/migrations --network host migrate/migrate -path=/migrations/ -database postgres://worker:qwerty@localhost:3457/ezil?sslmode=disable up

.PHONY: migrate_down
migrate_down:
	docker run -v ${PWD}/deploy/migrations/psql:/migrations --network host migrate/migrate -path=/migrations/ -database postgres://worker:qwerty@localhost:3457/ezil?sslmode=disable down

.PHONY: run_server
run_server:
	go run cmd/server/main.go

.PHONY: run_worker
run_worker:
	go run cmd/worker/main.go

.PHONY: lint
lint:
	golangci-lint run ./...

.PHONY: compose
compose:
	docker-compose up -d
