DO
$$
BEGIN

    CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

    CREATE OR REPLACE FUNCTION updated_at_column()
        RETURNS TRIGGER
    AS $func$ BEGIN
            NEW.updated_at = now();
RETURN NEW;
END;
    $func$ language 'plpgsql';

CREATE TABLE IF NOT EXISTS manufacturer (
    uuid           uuid    DEFAULT gen_random_uuid()  PRIMARY KEY,
    details        jsonb                              NOT NULL,
    created_at                  timestamp             NOT NULL        DEFAULT now(),
    updated_at                  timestamp,
    deleted_at                  timestamp
    );

CREATE TRIGGER updated_at_trigger BEFORE UPDATE
    ON manufacturer FOR EACH ROW EXECUTE PROCEDURE updated_at_column();
END;
$$;