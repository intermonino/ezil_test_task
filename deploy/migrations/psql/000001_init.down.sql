DO
$$
BEGIN

DROP TRIGGER IF EXISTS updated_at_trigger on manufacturer;

DROP TABLE manufacturer;

DROP FUNCTION IF EXISTS updated_at_column();

DROP EXTENSION IF EXISTS "uuid-ossp";

END;
$$;