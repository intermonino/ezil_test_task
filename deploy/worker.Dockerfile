FROM golang:alpine as build

RUN apk add git gcc

WORKDIR /build

COPY go.mod go.sum ./
RUN  go mod download

COPY cmd/worker      cmd/worker
COPY internal/    internal/
COPY pkg/  pkg/


RUN cd /build/cmd/worker && \
    go build -o /bin/worker

FROM alpine:latest
LABEL maintainer="Antony Martynov <intermonino@gmail.com> tg:@ntnmrtnv"
LABEL service="Worker"

COPY --from=build /bin /bin

WORKDIR /bin
CMD /bin/worker