FROM golang:alpine as build

RUN apk add git gcc

WORKDIR /build

COPY go.mod go.sum ./
RUN  go mod download

COPY cmd/migrator      cmd/migrator
COPY internal/    internal/
COPY pkg/  pkg/
COPY deploy/  deploy/

RUN cd /build/cmd/migrator && \
    go build -o /bin/migrator

FROM alpine:latest
LABEL maintainer="Antony Martynov <intermonino@gmail.com> tg:@ntnmrtnv"
LABEL service="Migrator"

COPY --from=build /bin /bin

WORKDIR /bin
CMD ["sh", "/bin/migrator", "up"]