# Ezil test task

## Условие

```В базе данных pgsql хранятся производители

manufacturer ( id varchar, details jsonb)

В поле details есть json в котором в корне структуры есть поле isNeedUpdate (bool)

Написать скрипт который бы доставал тех производителей у которых isNeedUpdate=true

батчами, по N элементов и отправлял их в http rest api (которое нужно тоже написать), получал подтверждение  от api и помечал  как isNeedUpdate false в базе.

Будет плюсом:

- docker-compose для решения
- замер латенси ответов http rest api и запросов в pgsql
- настроенный dashboard для этих метрик```

## О реализации
1) Worker - демон, который генерирует новые manufacturer, а так же обновляет manufacturer isNeedUpdate=true.
2) Server - сервер, который принимает http запросы от worker на обновление сущностей manufacturer. Выставляет во входящих сущностях isNeedUpdate=false
и возвращает их же в виде ответа.
3) Композиция сервисов для сборки метрик мониторинга:
   - Node exporter - извлекает метрики с хоста.
   - Postgres exporter - извлекает метрики с инстанса бд psql.
   - Prometheus - собирает метрики с экспортеров и worker.
   - Grafana - отображение метрик.

## Карта сервисов
![img_1.png](img_1.png)

## Запуск

В репозитории поставляется Makefile.

Для запуска всего ансамбля сервисов в docker-compose:
```shell
make compose
```

Для запуска server локально без docker:
```shell
make run_server
```

Для запуска worker локально без docker:
```shell
make run_worker
```

Для запуска миграций локально без docker:
```shell
make migrate_up
```

Для запуска golangci-lint:
```shell
make lint
```

## Переменные окружения

| Название           | тип           | default      | Описание
|--------------------|---------------|--------------|--------------------------------------
| SERVER_HOST        | string        | 0.0.0.0      | Адресс слушает сервер
| SERVER_PORT        | string        | 8080         | Порт на котором слушает сервер
| UPDATE_DURATION    | time.Duration | 30s          | Интервал работы функции обновления воркера
| GENERATE_DURATION    | time.Duration | 15s          | Интервал работы функции генерации воркера
| BATCH_SIZE | int           | 50           | Размер батча на обновление
| METRICS_PORT          | string        | 5050         | Порт на котором весит сервер с метриками
| ENABLE_PROFILING        | bool          | true         | Флаг включения ручек для профилирования
| DEBUG        | bool          | true         | Флаг уровня логирвания Debug
| DBHost    | string        | 0.0.0.0:3457 | Адресс на которой слушает psql
| DBUser    | string        | worker       | Имя пользователя
| DBPass    | string        | qwerty       | Пароль пользователя
| DBSSL    | string        | disable      | SSL
| DBName    | string        | ezil         | Название БД
