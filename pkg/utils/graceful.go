package utils

import (
	"context"
	"os"
	"os/signal"
	"syscall"
)

func InitGracefulShutdown(cancelFunc context.CancelFunc) {
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	<-done
	cancelFunc()
}
